const express = require('express');
const cors = require('cors');
const routes = require('./Routes/routes')
const path = require('path')
const bodyParser = require('body-parser')
const dotenv = require('dotenv')

dotenv.config()

const mongoose = require('mongoose')

mongoose.connect("mongodb://localhost:27017/NodeJS",
                {
                    useUnifiedTopology: true,
                    useNewUrlParser: true
                })
                .then(()=>{
                    console.log('DB connected')
                })
                .catch(err=>{
                    console.log(err)
                })
const app = express();
app.use(cors());
app.use(bodyParser.json()); 
app.use("/api", routes);

const PORT = process.env.PORT;

app.listen(PORT,(()=>{
    console.log('Server started at',PORT);
}));