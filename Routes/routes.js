const express = require('express');
const logCtrl = require('../src/ctrl/auth'); 
const auth = require('../utils/auth/auth');

router = express.Router();

router.post("/auth", logCtrl.login);

router.put("/password/reset", auth, changePassword)

module.exports = router;   

