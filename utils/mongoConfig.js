const mongoose = require('mongoose')

mongoose.connect("mongodb://localhost:27017/NodeJS",
                {
                    useUnifiedTopology: true,
                    useNewUrlParser: true
                })
                .then(()=>{
                    console.log('DB connected')
                })
                .catch(err=>{
                    console.log(err)
                })