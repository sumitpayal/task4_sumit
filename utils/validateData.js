const Ajv = require('ajv');  

const validateSchema = (schema, input) => { 
    input = input || {};

    const ajv = new Ajv({
        allErrors: true,
    }); 

    const validate = ajv.compile(schema);
 
    const valid = ajv.validate(input);

    if (!valid) {
        throw(ajv.errorsText(ajv.errors));
    } 
    return null;
}

module.exports = validateSchema;