const dotenv = require('dotenv');
const error = require('../src/error')

dotenv.config();

module.exports = async(req,res,next) => {
    const token = req.headers.authorization; 
    try{
        return verifyAuthToken(token, process.env.JWT_SECRET_KEY);
    }
    catch(err){
        res.json({
            message: err
        })
    }
}