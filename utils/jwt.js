const jwt = require('jsonwebtoken');

//Create JWT
const createToken = (payload, secretKey, options) => {
    return jwt.sign(
        payload,
        secretKey,
        options
    );
};

//Verify Token
const verifyToken = (token, secretKey) => {
    return new Promise((resolve, reject) => {
        jwt.verify(token, secretKey, (err, decoded) => {
            if(err)
                return reject(err);
            return resolve(decoded);
        })
    })
};

//Create Token on login
const createAuthToken = (user, secretKey, expiresIn) => {
    return createToken(
        { user: user }, 
        secretKey,
        { expiresIn: expiresIn } 
    );
}

//Verify Token on login
const verifyAuthToken = (token, secretKey) => {
    return verifyToken(token, secretKey);
}

module.exports = {
    createAuthToken,
    verifyAuthToken,
}