module.exports ={
    LOGIN_SUCCESS :{
        status: 200,
        code: 'LOGIN_SUCCESS',
        details: 'Login successfully.',
        desc: 'OK'
    },
    PASSWORD_UPDATE_SUCCESS :{
        status: 200,
        code: 'PASSWORD_UPDATE_SUCCESS',
        details: 'Password updated successfully.',
        desc: 'OK'
    }
}