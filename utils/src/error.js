module.exports ={
    INVALID_INPUT :{
        status: 400,
        code: 'INVALID_INPUT',
        details: 'The request input is not as expexted.',
        desc: 'Bad Request'
    },
    INVALID_TOKEN :{
        status: 401,
        code: 'INVALID_TOKEN',
        details: 'Access denied. Current user does not has access for this resource.',
        desc: 'Auth Token Invalid'
    },
    PASSWORD_MISMATCH: {
        status: 401,
        code: 'PASSWORD_MISMATCH',
        detail: 'Your password does not match.',
        desc: 'Unauthorized'
    },
    ID_NOT_FOUND: {
        status: 401,
        code: 'ID_NOT_FOUND',
        detail: 'Your id not found.',
        desc: 'Unauthorized'
    },
    INTERNAL_ERROR: {
        status: 500,
        code: 'INTERNAL_ERROR',
        detail: 'An unexpected internal error has occurred. Please contact Support for more information.',
        desc: 'Internal Server Error'
    },
    OTP_MISMATCH: {
        status: 401,
        code: 'OTP_MISMATCH',
        detail: 'Your otp did not match.',
        desc: 'Unauthorized'
    },
}