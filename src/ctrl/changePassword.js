const validateSchema = require('../../utils/validateData');
const User  = require('../../Models/user');
const error = require('../../utils/src/error');
const success = require('../../utils/src/success');
const dotenv = require('dotenv');

dotenv.config();

const passwordSchema = {
    type: 'object',
    properties: {
        'oldPassword': {
            type: 'string',
            minLength: 5
        },
        'newPassword': {
            type: 'string',
            minLength: 5
        }
    },
    required: ['oldPassword', 'newPassword'],
    additionalProperties: false
};

exports.changePassword = async(req,res,next) =>{
    let email = req.body.email;
    try{
        validateSchema(passwordSchema, req.body);
        const userData = await User.countDocuments({email : email});
        if(userData){
            const user = await User.findOne({email : email}); 
            if(user.password !== req.body.oldPassword){
                throw(error.PASSWORD_MISMATCH);
            }
            User.updateOne({email:email},{$set:{password: req.body.newPassword}})
                .then(()=>{
                    res.json({
                        message: success.PASSWORD_UPDATE_SUCCESS
                    })
                })
                .catch(()=>{
                    res.json({
                        message: error.INTERNAL_ERROR
                    })
                })
        }
    }
    catch(err){
        res.json({
            message: err
        })
    }
}