const validateSchema = require('../../utils/validateData');
const User  = require('../../Models/user');
const error = require('../../utils/src/error');
const success = require('../../utils/src/success');
const dotenv = require('dotenv')
const sgMail = require('@sendgrid/mail')

dotenv.config();

const loginSchema = {
    type: 'object',
    properties: {
        'email': {
            type: 'string'
        },
        'password': {
            type: 'string',
            minLength: 5
        }
    },
    required:['email','password'],
    additionalProperties: false
};

exports.login = async(req,res,next) => { 
    let email = req.body.email;
    try{
        validateSchema(loginSchema, req.body); 
        const userData = await User.countDocuments({email : email});
        if(userData){
            const user = await User.findOne({email : email}); 
            if(user.password !== req.body.password){
                throw(error.PASSWORD_MISMATCH);
            }
    
            // sgMail.setApiKey(process.env.SENDGRID_API_KEY);
            // const msg = {
            //     to: email,
            //     from: process.env.SENDER_MAIL, // Use the email address or domain you verified above
            //     subject: 'Sending with Twilio SendGrid is Fun',
            //     text: 'and easy to do anywhere, even with Node.js',
            //     html: '<strong>and easy to do anywhere, even with Node.js</strong>',
            // };

            // sgMail.send(msg)
            //       .then(()=>{},error =>{
            //           console.log(error);

            //           if (error.response) {
            //             console.error(error.response.body)
            //           }
            //       });

            res.json({
                message: success.LOGIN_SUCCESS
            })
        }
        throw(error.ID_NOT_FOUND);
    }
    catch(err){
        return res.json({
            message: err
        })
    }
}